import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LevelregisterComponent } from './technology/levelregister/levelregister.component';
import { UserregisterComponent } from './user/userregister/userregister.component';

const routes: Routes = [
  {path:"register",component:UserregisterComponent},
  {path:"registerLevel",component:LevelregisterComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
