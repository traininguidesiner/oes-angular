import { HttpInterceptorAuthService } from './core/http-interceptor-auth.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserModule } from './user/user.module';
import { TechnologyModule } from './technology/technology.module';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';
import { ModalModule } from 'ngx-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    Ng4LoadingSpinnerModule.forRoot(),
    BrowserModule,
    NgSelectModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    UserModule,
    TechnologyModule,
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot(),
    ModalModule.forRoot(),
  ],
  providers: [{ provide: HTTP_INTERCEPTORS, useClass: HttpInterceptorAuthService, multi: true },],
  bootstrap: [AppComponent]
})
export class AppModule { }
