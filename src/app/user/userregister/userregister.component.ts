import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { RegisterService } from './register.service';

@Component({
  selector: 'app-userregister',
  templateUrl: './userregister.component.html',
  styleUrls: ['./userregister.component.css']
})
export class UserregisterComponent implements OnInit {

  constructor(private service:RegisterService) {
    this.registrationForm = new FormGroup(this.registrationFormValidation);
   }

  ngOnInit() {
  }

  registrationForm:FormGroup;
  registrationFormValidation={
    firstName: new FormControl('', [Validators.required, Validators.pattern("[A-Za-z]*")]),
    lastName: new FormControl('', [Validators.required, Validators.pattern("[A-Za-z]*")]),
    email: new FormControl('', [Validators.email]),
    password: new FormControl('', [Validators.required, Validators.pattern("[A-Za-z0-9]*")]),
    phoneNumber: new FormControl('', [Validators.pattern("[0-9]{10}")]),
    userType:new FormControl("user")
  }

  submitUser() {
    console.log(this.registrationForm.value);
    this.service.saveUser(this.registrationForm.value).subscribe(res=> {
      console.log(res);
      alert('User Registered Successfully');
    }, error=> {
      console.log(error);
      alert('Please check the server');
    })
  }
}
