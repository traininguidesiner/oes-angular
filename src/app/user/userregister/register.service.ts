import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Environment } from 'src/app/core/environment';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  constructor(private http:HttpClient) { }

  env = new Environment();

  public saveUser(payload) {
    return this.http.post(this.env.url+'RegisterUser', payload);
  }

  public getUserById(userId) {
    return this.http.post(this.env.url+'GetByUserId?userId=', userId);
  }
}
