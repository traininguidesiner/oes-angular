import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserregisterComponent } from './userregister/userregister.component';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { UserLoginComponent } from './user-login/user-login.component';

@NgModule({
  declarations: [
    UserregisterComponent,
    UserLoginComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ]
})
export class UserModule { }
