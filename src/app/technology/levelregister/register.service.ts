import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Environment } from 'src/app/core/environment';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  constructor(private http:HttpClient) { }

  env = new Environment();

  public saveLevel(payload) {
    return this.http.post(this.env.url+'RegisterLevel',payload);
  }

  public getLevelById(levelId) {
    return this.http.get(this.env.url+'GetByLevelId?levelId='+levelId);
  }

  public getAllLevels() {
    return this.http.get(this.env.url+"/GetAllLevels");
  }
}
