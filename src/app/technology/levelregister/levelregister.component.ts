import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { RegisterService } from './register.service';


@Component({
  selector: 'app-levelregister',
  templateUrl: './levelregister.component.html',
  styleUrls: ['./levelregister.component.css']
})
export class LevelregisterComponent implements OnInit {
  data:any[] = [];
  constructor(private service: RegisterService) {
    this.levelRegistrationForm = new FormGroup(this.levelRegistrationFormValidation);
    this.getAllLevelsData();
  }

  ngOnInit() {
  }

  levelRegistrationForm: FormGroup;
  levelRegistrationFormValidation = {
    levelName: new FormControl('', [Validators.required, Validators.pattern("[A-Za-z]*")]),
    levelDescription: new FormControl()
  }

  submitLevel() {
    console.log(this.levelRegistrationForm.value);
    this.service.saveLevel(this.levelRegistrationForm.value).subscribe(res => {
      console.log(res);
      this.getAllLevelsData();
      alert('Level Registered Successfully');
    }, error => {
      console.log(error);
      alert('Please check the server');
    })
  }

  getAllLevelsData() {
    this.service.getAllLevels().subscribe(res=> {
      console.log(res);
      this.data = res['result'];
      console.log(this.data);
    })
  }

}
