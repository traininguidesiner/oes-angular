import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LevelregisterComponent } from './levelregister.component';

describe('LevelregisterComponent', () => {
  let component: LevelregisterComponent;
  let fixture: ComponentFixture<LevelregisterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LevelregisterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LevelregisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
