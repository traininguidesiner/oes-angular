import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LevelregisterComponent } from './levelregister/levelregister.component';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [LevelregisterComponent],
  imports: [
    CommonModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ]
})
export class TechnologyModule { }
